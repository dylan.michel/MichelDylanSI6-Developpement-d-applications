bin/main: obj/main.o obj/calcul.o obj/affichage.o
	g++ $(shell pkg-config --libs libpq) -o bin/main obj/main.o obj/affichage.o obj/calcul.o

obj/main.o: src/main.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/main.o -c src/main.cpp

obj/affichage.o: src/affichage.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/affichage.o -c src/affichage.cpp

obj/calcul.o: src/calcul.cpp
	g++ $(shell pkg-config --cflags libpq) -o obj/calcul.o -c src/calcul.cpp



clean:
	-rm obj/* bin/*
